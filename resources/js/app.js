require('./bootstrap');

window.Vue = require('vue').default;

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('v-header', require('./components/Header.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('multiselect', require('vue-multiselect').default);
import router from "./router";
import store from "./store";
import moment from "moment";



const app = new Vue({
    el: '#app',
    router,
    store,
    moment,
});

