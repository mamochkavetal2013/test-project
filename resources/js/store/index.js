import Vue from 'vue';
import Vuex from 'vuex';
import StoreStatistic from './statistic';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        statistic: StoreStatistic,
    },
})
