import vueRouter from 'vue-router';
import Vue from 'vue';
import store from './store';

Vue.use(vueRouter);

import AllOrder from "./views/orders/AllOrder";
import CreateOrder from "./views/orders/CreateOrder";

const routes = [
    {
        path: '/',
        name: 'orders',
        component: AllOrder,

    },
    {
        path: '/orders/create',
        name: 'order-create',
        component: CreateOrder,

    },
];

const router = new vueRouter({
    mode: 'history',
    routes,
});


export default router
