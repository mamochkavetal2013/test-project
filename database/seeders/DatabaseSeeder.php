<?php

namespace Database\Seeders;

use App\Models\Manager;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Order::factory()
            ->count(25)
            ->create();

        Product::factory()
            ->count(25)
            ->for(Manager::factory())
            ->create();
    }
}
