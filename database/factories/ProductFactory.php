<?php

namespace Database\Factories;

use App\Models\Manager;
use Illuminate\Database\Eloquent\Factories\Factory;

final class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word(),
            'descriptions' => $this->faker->paragraph(),
            'price' => $this->faker->randomFloat(2),
            'manufacture' => $this->faker->word(),
            'manager_id' => Manager::factory(),

        ];
    }
}
