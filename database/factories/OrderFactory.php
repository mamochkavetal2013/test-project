<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Stevebauman\Location\Facades\Location;

final class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $ip = $this->faker->ipv4();

        return [
            'product_id' => Product::factory(),
            'name' => $this->faker->name(),
            'phone' => $this->faker->phoneNumber(),
            'email' => $this->faker->unique()->safeEmail(),
            'geo' => $this->faker->longitude(),
            'ip' => $ip,
        ];
    }
}
