<?php

use App\Http\Controllers\API\ManagerController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::apiResource('orders', OrderController::class)
    ->only(['index','store']);

Route::apiResource('products', ProductController::class)
    ->only(['index']);

Route::apiResource('managers', ManagerController::class)
    ->only(['index']);
