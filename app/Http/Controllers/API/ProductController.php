<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ProductResource;
use App\Models\Product;

final class ProductController extends BaseController
{
    public function index()
    {
        return $this->sendResponse(
            ProductResource::collection(Product::all()),
            'Orders retrieved successfully.'
        );
    }


}
