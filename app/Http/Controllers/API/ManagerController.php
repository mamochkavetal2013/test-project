<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ManagerResource;
use App\Models\Manager;

final class ManagerController extends BaseController
{

    public function index()
    {
        return $this->sendResponse(
            ManagerResource::collection(Manager::get()),
            'Orders retrieved successfully.'
        );
    }


}
