<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Filters\OrderFilter;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Http\Request;
use Validator;

final class OrderController extends BaseController
{
    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }


    public function index(OrderFilter $filter)
    {
        $orders = Order::filter($filter)
            ->with('product.manager')
            ->paginate(3);

        return $this->sendResponse(
            $orders,
            'Orders retrieved successfully.'
        );
    }


    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|email|unique:orders,email',
            'product_id' => 'required|exists:products,id',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors(), 422);
        }


        $order = $this->orderService->create($input + [
            'ip' => $request->ip()
            ]);

        return $this->sendResponse(new OrderResource($order), 'Order created successfully.');
    }


}
