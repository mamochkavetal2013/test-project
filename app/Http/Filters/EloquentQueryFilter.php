<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class EloquentQueryFilter
 * @package App\Http\Filters
 * @property Builder $builder
 */
abstract class EloquentQueryFilter extends BaseFilter
{
    /**
     * Sort the collection by the sort field
     * Examples: sort= title,-status || sort=-title || sort=status
     *
     * @param string $value
     */
    protected function sort(string $value)
    {
        collect(explode(',', $value))->mapWithKeys(function (string $field) {
            switch (substr($field, 0, 1)) {
                case '-':
                    return [substr($field, 1) => 'desc'];
                case ' ':
                    return [substr($field, 1) => 'asc'];
                default:
                    return [$field => 'asc'];
            }
        })->each(function (string $order, string $field) {
            $this->builder->orderBy($field, $order);
        });
    }
}
