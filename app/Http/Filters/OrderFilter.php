<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class OrderFilter extends EloquentQueryFilter
{

    public function productIds(array $products) {
        return $this->builder->whereIn('product_id', $products);

    }

    public function managerIds(array $managers) {
        return $this->builder->whereHas('product.manager', function (Builder $query) use ($managers) {
            return $query->whereIn('id', $managers);
        });
    }

}
