<?php

namespace App\Http\Filters;

use stdClass;

interface FilterInterface
{
    /**
     * @param stdClass $builder Custom query builder
     * @return mixed
     */
    public function apply($builder);

    /**
     * @return array of fields
     */
    public function fields(): array;
}
