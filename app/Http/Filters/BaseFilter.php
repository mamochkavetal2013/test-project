<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Str;

/**
 * Class BaseFilter
 * @package App\Http\Filters
 */
abstract class BaseFilter extends FormRequest implements FilterInterface
{
    /** @var Request */
    public $request;

    /** @var Builder $builder */
    protected $builder;

    /** @var array */
    protected $filters = [];

    /**
     * @var array
     */
    protected $denied_methods = ['apply','fields'];

    public function fields(): array
    {
        $requestFilters = $this->request->get('filter', []);

        if (is_string($requestFilters)) {
            $requestFilters = json_decode($requestFilters, true);
        }

        if (!is_array($requestFilters)) {
            $requestFilters = [];
        }
        $requestFilters['sort'] = $this->request->get('sort', 'id');
        return array_merge(
            $requestFilters, $this->filters
        );
    }

    /**
     * Each model will run this apply function from his scope
     *
     * @param Builder $builder
     */
    public function apply($builder)
    {
        $this->builder = $builder;

        // GET /entity?title=source&status=publish&interests_in=<id>&sort=-title,age
        foreach ($this->fields() as $field => $value) {
            $method = Str::camel($field);

            if (method_exists($this, $method) && !in_array($method, $this->denied_methods) && $value) {
                call_user_func_array([$this, $method], [$value]);
            }
        }
    }

    public function setFilter($key, $value) {
        $this->filters[$key] = $value;
    }

    public function rules() {
        return [];
    }
}
