<?php

namespace App\Traits\Models\Eloquent;

use App\Http\Filters\EloquentQueryFilter;
use Illuminate\Database\Eloquent\Builder;

/**
 * Trait Filterable
 * @package App\Traits\Models\Eloquent
 *
 * @method static Builder filter($filter)
 */
trait Filterable
{
    /**
     * @param Builder $builder
     * @param EloquentQueryFilter $filter
     */
    public function scopeFilter(Builder $builder, EloquentQueryFilter $filter)
    {
        $filter->apply($builder);
    }
}
