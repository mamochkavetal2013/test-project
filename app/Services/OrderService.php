<?php

namespace App\Services;


use App\Helpers\CustomHelper;
use App\Models\Order;
use App\Services\Base\MySqlService;
use DB;
use Exception;
use Stevebauman\Location\Facades\Location;


final class OrderService extends MySqlService
{
    /**
     * @param array $data
     * @throws Exception
     */

    public function create(array $data)
    {
        DB::beginTransaction();
        try {

            $order = Order::make($data);
            $order->geo = Location::get("5.180.100.180");

            if (isset($data['ip'])) {
                $location = Location::get($data['ip']);
                $order->geo = json_encode([$location->longitude, $location->latitude]);
            }

            if (isset($data['product_id'])) {
                $order->product()->associate($data['product_id']);
            }

            $order->save();

            DB::commit();

            return $order;
        } catch (Exception $exception) {

            $this->handleException($exception);
        }
    }


}
